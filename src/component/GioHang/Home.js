import React, { Component } from "react";
import ProductList from "./ProductList";
import ProductDetail from "./ProductDetail";
import Cart from "./Cart";

export class Home extends Component {
  products = [
    {
      id: 1,
      name: "TP Thể thao",
      alias: "thể thao",
      price: 350,
      description:
        "Chất vải cotton co giãn thấm hút mồ hôi cực tốt",
      quantity: 995,
      image: "https://product.hstatic.net/1000341630/product/mid07774_74e3354ac7784e34951dab16eda45e0f_master.jpg",
    },
    {
      id: 2,
      name: "TP Xuân hè",
      alias: "xuân hè",
      price: 450,
      description:
        "Vải KaKi cao cấp kết hợp với vải thun cotton 100% cao cấp cho bé trai",
      quantity: 990,
      image: "https://salt.tikicdn.com/ts/product/9e/8a/8c/2529aa72a2588530cde63c0739ecd3e2.jpg",
    },
    {
      id: 3,
      name: "Váy xuân hè",
      alias: "xuân hè",
      price: 375,
      description:
        "Vải voan cao cấp 2 lớp tạo độ bồng bềnh cho bé gái",
      quantity: 415,
      image: "https://toplistdanang.com/wp-content/uploads/2019/11/shop-ao-quan-tre-em-da-nang-5_optimized.jpg",
    },
    {
      id: 4,
      name: "Áo thun",
      alias: "áo thun",
      price: 465,
      description:
        "Chất liệu 100% cotton cho cả bé trai và bé gái",
      quantity: 542,
      image: "https://png.pngtree.com/element_our/20200610/ourlarge/pngtree-children-s-short-sleeve-image_2238009.jpg",
    },
    {
      id: 5,
      name: "Váy đi tiệc",
      alias: "váy đi tiệc",
      price: 550,
      description:
        "Chất liệu voan cao cấp, nhiều lớp tạo độ bồng bềnh cho váy",
      quantity: 674,
      image: "https://tse1.mm.bing.net/th?id=OIP.xy8IpwHit1m6Kv-RGror3AHaHa&pid=Api&P=0&h=180",
    },
    {
      id: 6,
      name: "Đồ đi tiệc",
      alias: "Đồ đi tiệc cho bé trai",
      price: 250,
      description:
        "Quần tây kết hợp áo sơ mi và ghi lê tạo độ lãng tử cho bé trai",
      quantity: 456,
      image: "https://tse2.mm.bing.net/th?id=OIP.Q5xfMLT9ajWdEyWui4pMxgHaHa&pid=Api&P=0&h=180",
    },
    {
      id: 7,
      name: "Đồ thu đông",
      alias: "Đồ thu đông cho bé trai và bé gái",
      price: 450,
      description:
        "Chất liệu nỉ ấm áp giúp giữ nhiệt cơ thể cho cả bé trai và bé gái",
      quantity: 854,
      image: "https://tse3.mm.bing.net/th?id=OIP.uZwdgaBFrizvBtR2ixRSagHaLH&pid=Api&P=0&h=180",
    },
    {
      id: 8,
      name: "Đồ bơi",
      alias: "Đồ bơi cho bé trai",
      price: 750,
      description:
        "Đồ bơi cho bé trai ôm cơ thể, dễ dàng hoạt động dưới nước",
      quantity: 524,
      image: "https://bizweb.dktcdn.net/100/032/149/products/do-boi-tay-dai-cho-be-trai-size-dai.jpg?v=1677597827207",
    }
  ];
  state = {
    selectedProduct: null,
    cart: [],
  };
  renderProduct = (prod) => {
    this.setState({
      selectedProduct: prod,
    });
  };
  addToCart = (prod) => {
    const cloneCart = [...this.state.cart];

    const foundItem = cloneCart.find((item) => {
      return item.product.id === prod.id;
    });

    if (foundItem) {
      foundItem.quantity += 1;
    } else {
      //? thêm mới sản phẩm
      const cartItem = {
        product: prod,
        quantity: 1,
      };
      cloneCart.push(cartItem);
    }
    this.setState(
      {
        cart: cloneCart,
      },
      () => {
        console.log(this.state.cart);
      }
    );
  };
  delete = (id) => {
    const cloneCart = [...this.state.cart];
    const index = cloneCart.findIndex((item) => item.id === id);

    if (index !== -1) {
      cloneCart.splice(index, 1);
    }
    this.setState({
      cart: cloneCart,
    });
  };
  render() {
    return (
      <div className="container">
        <h5 className="display-4">CÚN * CÒ  SHOP</h5>
        <button className="text-right btn-success mb-3" data-toggle="modal" data-target="#exampleModal">
        <i class="fa-solid fa-cart-shopping"></i> ({this.state.cart.length})
        </button>
        <ProductList
          renderProduct={this.renderProduct}
          product={this.products}
          addToCart={this.addToCart}
        />
        {this.state.selectedProduct && (
          <ProductDetail selectedProduct={this.state.selectedProduct} />
        )}
        <Cart
          cart={this.state.cart}
          delete={this.delete}
        />
      </div>
    );
  }
}

export default Home;
