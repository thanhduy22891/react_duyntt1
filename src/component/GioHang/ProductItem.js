import React, { Component } from "react";

export class ProductItem extends Component {
  render() {
    const { name, price, image } = this.props.prod;
    return (
      <div>
        <div className="card p-3 h-100">
          <img src={image} className="w-100 d-block" alt="" />
          <h3>{name}</h3>
          <h5>{price}$</h5>
          <div>
            <button
              className="btn btn-info mr-2"
              onClick={() => this.props.renderProduct(this.props.prod)}
            >
              Detail
            </button>
            <button
              onClick={() => this.props.addToCart(this.props.prod)}
              className="btn btn-dark"
            >
              Add to Cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductItem;
