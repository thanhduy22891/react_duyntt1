import React, { Component } from 'react'

export default class extends Component {
    render() {
        return (
            <>
                {/* Footer */}
                <div className="container-fluid bg-dark text-light footer mt-5 py-5 wow fadeIn" data-wow-delay="0.1s">
                    <div className="container py-5">
                        <div className="row g-5">
                            <div className="col-lg-4 col-md-6">
                                <h4 className="text-white mb-4">Our Office</h4>
                                <p className="mb-2"><i className="fa fa-map-marker-alt me-3"></i>70 Trịnh Lỗi, Ngũ Hành Sơn</p>
                                <p className="mb-2"><i className="fa fa-phone-alt me-3"></i>0796 862 700</p>
                                <p className="mb-2"><i className="fa fa-envelope me-3"></i>thanhduy@gamil.com</p>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
