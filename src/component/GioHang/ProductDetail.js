import React, { Component } from "react";

export class ProductDetail extends Component {
  render() {
    const { name, image, description, quantity, price } =
      this.props.selectedProduct;
    return (
      <div className="row mt-3">
        <div className="col-4">
          <h1 className="display-4 font-weight-normal">{name}</h1>
          <img src={image} className="w-100" alt="" />
        </div>
        <div className="col-8">
          <h5 className="display-4 font-weight-normal">THÔNG TIN SẢN PHẨM</h5>
          <p>Name: {name}</p>
          <p>Price: {price}</p>
          <p>Description: {description}</p>
          <p>Stock: {quantity}</p>
        </div>
      </div>
    );
  }
}

export default ProductDetail;
