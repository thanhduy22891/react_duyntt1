import React, { Component } from 'react'


export default class Header extends Component {
    render() {
        return (
            <>
                {/* Topbar */}

                <div className="container-fluid bg-dark text-light px-0 py-2">
                    <div className="row gx-0 d-none d-lg-flex">
                        <div className="col-lg-7 px-5 text-start">
                            <div className="h-100 d-inline-flex align-items-center me-4">
                                <span className="fa fa-phone-alt me-2"></span>
                                <span>0796 862 700</span>
                            </div>
                            <div className="h-100 d-inline-flex align-items-center">
                                <span className="far fa-envelope me-2"></span>
                                <span>thanhduy@gmail.com</span>
                            </div>
                        </div>
                        <div className="col-lg-5 px-5 text-end">
                            <div className="h-100 d-inline-flex align-items-center mx-n2">
                                <span> <i class="fa-solid fa-location-dot"></i> 70 Trịnh Lỗi, Ngũ Hành Sơn</span>
                            </div>
                        </div>
                    </div>
                </div>


            </>
        )
    }
}
