import React, { Component } from "react";

export class Cart extends Component {
  render() {
    return (
      <div>
        <div
          class="modal"
          id="exampleModal"
          tabindex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                  Cart
                </h5>
                <button
                  type="button"
                  class="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <table className="table">
                  <thead>
                    <tr>
                      <th>Product ID</th>
                      <th>Name</th>
                      <th>Image</th>
                      <th>Quantity</th>
                      <th>Price</th>
                      <th>Total</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.props.cart.map((item) => (
                      <tr key={item.id}>
                        <td>{item.product.id}</td>
                        <td>{item.product.name}</td>
                        <td>
                          <img src={item.product.image} alt="" width={60} />
                        </td>
                        {/* <td>
                          <button
                            className="btn btn-info"                            
                          >
                            -
                          </button>
                          <span> {item.quantity} </span>
                          <button
                            className="btn btn-info"
                          >
                            +
                          </button>
                        </td> */}
                        <td>{item.product.price}</td>
                        <td>{item.quantity * item.product.price} $</td>
                        <td>
                          <button
                            className="btn btn-danger"
                            onClick={() => this.props.delete(item.id)}
                          >
                            Delete
                          </button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
              <div class="modal-footer">
                <button
                  type="button"
                  class="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" class="btn btn-primary">
                  Save changes
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Cart;
