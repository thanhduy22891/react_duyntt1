import React, { Component } from "react";
import ProductItem from "./ProductItem";

export class ProductList extends Component {
  render() {
    return (
      <div className="row">
        {this.props.product.map((item) => (
          <div key={item.id} className="col-3">
            <ProductItem
              renderProduct={this.props.renderProduct}
              prod={item}
              addToCart={this.props.addToCart}
            />
          </div>
        ))}
      </div>
    );
  }
}

export default ProductList;
