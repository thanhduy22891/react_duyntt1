import logo from './logo.svg';
import './App.css';
import Home from './component/GioHang/Home'
import Header from './component/GioHang/Header';
import Footer from './component/GioHang/Footer';

function App() {
  return (
    <div className="App">
      <Header/>
      <Home/>
      {/* <Routes>
        <Route path='/' element={<Body/>}></Route>
        <Route path='/sanpham' element={<Home/>}></Route>
      </Routes> */}
      <Footer/>
    </div>
  );
}

export default App;
